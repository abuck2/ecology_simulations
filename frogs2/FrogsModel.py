from mesa import Agent, Model
#Space grid
from mesa.space import MultiGrid
#Scheduler
from mesa.time import RandomActivation
#Data collection
from mesa.datacollection import DataCollector


from numpy import random
import matplotlib.pyplot as plt
import numpy as np
import logging
from scipy.ndimage import gaussian_filter

from Agent import Frog

def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)
    
def compute_population_f(model):
    return len([agent for agent in model.schedule.agents if isinstance(agent, Frog)])

def compute_genet(model):
    frogs = [agent for agent in model.schedule.agents if isinstance(agent, Frog)]
    L = len([agent for agent in frogs if agent.genet=="LL"])
    R = len([agent for agent in frogs if agent.genet=="RR"])
    N = len(frogs)
    
    return (L+R)/N

def compute_mt(model):
    return 0

class FrogsModel(Model):
    def __init__(self, n_l:int, n_r:int, n_h:int, width:int, height:int, 
            r_rate:float = 0.5, pref_mat:float = 0.5,
            life_exp:int = 10, hybridogenesis:bool=1, hybrid_crosses:bool=1):
        super().__init__()
        
        
        #Number of starting frogs : lessonae, ridibundus or hybrid
        self.n_l = n_l
        self.n_r = n_r
        self.n_h = n_h
        
        self.current_step = 0
        
        #set as model attributes for vizualisation purposes
        self.r_rate = r_rate
        self.pref_mat = pref_mat
        self.life_exp = life_exp
        self.hybridogenesis = hybridogenesis
        self.hybrid_crosses = hybrid_crosses
        
        #Create space
        self.grid = MultiGrid(width, height, True)
        
        #Activation of the agents every step is random
        self.schedule = RandomActivation(self)
        
        #Initialize loggers 
        setup_logger('frogs_logger', "logs/local_frogs.log")
        frogs_logger = logging.getLogger('frogs_logger')
        
        self.datacollector = DataCollector(
                model_reporters={
                    "Frogs":compute_population_f, 
                    "Introg":compute_genet,
                    "Mixed":compute_mt
                    }) #agent-level data
        
        # Create local frogs 
        for i in range(self.n_l):
            a = Frog(unique_id = i, model = self, sex = i % 2, logger = frogs_logger,\
                genet = "LL", life_exp = self.life_exp, reprod_rate = self.r_rate)
            self.schedule.add(a)

            #Agent is activated in a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            pos = np.array((x, y))
            self.grid.place_agent(a, pos)

        for i in range(self.n_h):
            a = Frog(unique_id = i, model = self, sex = i % 2, logger = frogs_logger,\
                genet = "LR", life_exp = self.life_exp, reprod_rate = self.r_rate)
            self.schedule.add(a)

            #Agent is activated in a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            pos = np.array((x, y))
            self.grid.place_agent(a, pos)

        
            
        

    def step(self):
        """Avance the scheduler one step and collect data
        """
        print("Next step")
        #collect data
        self.datacollector.collect(self)
        #advance simulation one step
        self.schedule.step()
        self.current_step += 1

        #Introduces invasive at step 100
        if self.current_step == 100:
            for i in range(self.n_r):
                a = Frog(unique_id = i+self.num_frogs+1, model = self, sex = i % 2, logger = frogs_logger,\
                    genet = "RR", life_exp = self.life_exp, reprod_rate = self.r_rate)
                self.schedule.add(a)

                #Agent is activated in a random space cell
                x = self.random.randrange(self.grid.width)
                y = self.random.randrange(self.grid.height)
                pos = np.array((x, y))
                self.grid.place_agent(a, pos)








