from FrogsModel import *
from mesa.experimental import JupyterViz
import mesa
def agent_portrayal(agent):
    frog = {
        "color": "tab:green",
        "size": 50,
        "Layer":1,
    }
    if agent.genet == "RR":
        frog["color"]="tab:red"
    elif agent.genet == "LL" :
        frog["color"]="tab:blue"
    else :
        frog["color"]="tab:green"

    
    return frog
    
model_params = {
    "n_l": {
        "type": "SliderInt",
        "value": 50,
        "label": "Number of Lessona frogs:",
        "min": 10,
        "max": 200,
        "step": 1,
    },
    "n_r": {
        "type": "SliderInt",
        "value": 20,
        "label": "Number of Ridibundus frogs:",
        "min": 10,
        "max": 200,
        "step": 1,
    },
    "n_h": {
        "type": "SliderInt",
        "value": 20,
        "label": "Number of Hybrid frogs:",
        "min": 10,
        "max": 200,
        "step": 1,
    },
    "r_rate": {
        "type": "SliderInt",
        "value": 0.5,
        "label": "Reproduction rate of frogs:",
        "min": 0,
        "max": 1,
        "step": 0.05,
    },
    "pref_mat": {
        "type": "SliderInt",
        "value": 0.1,
        "label": "Preferential mating of hybrids (Not implemented)",
        "min": 0,
        "max": 1,
        "step": 0.05,
    },
    "life_exp": {
        "type": "SliderInt",
        "value": 20,
        "label": "Life expectancy of local frogs:",
        "min": 5,
        "max": 50,
        "step": 1,
    },
    "hybridogenesis": {
        "type": "Button",
        "value": 20,
        "label": "Life expectancy of alien frogs:",
    },
    "hybrid_crosses": {
        "type": "Button",
        "value": True,
        "label": "Life expectancy of alien frogs:",

    },
    "width": 20,
    "height": 20}


page = JupyterViz(
    FrogsModel,
    model_params,
    measures=["Introg"],
    name="Money Model",
    agent_portrayal=agent_portrayal,
)

