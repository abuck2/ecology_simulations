
# Model description, reporters and parameters

Parameters for the model follows : 
* Preferential mating of hybrid (0-1 float) : If > 0, Hybrid reproduce mostly with R - Left for later
* Hybridogenesis (bool) : If one Hybrid gametes are preferentially R
* "Reproduction rate" : This is not actually a reproduction rate, but a rate of successful encouters, and should therefore be relatively high
* Lessona starting number
* Ridibundus starting number
* Hybrid starting number : To simulate an initial L/E system
* hybrid_crosses (bool) : can hybrid reproduces with hybrids? Depending on the ridibundus origin, this might not always be the case.

Additional attributes :
* Grid : Toroid grid with set width and height
* Scheduler : A random scheduler

Reporters : 
* Total number of frogs
* Number of Ridibundus chromosomes on the total number of chromosomes

# Agents attributes and methods

## Agents attributes

* unique_id : Unique ID of the model
* life_exp : Number of turns after which an agent dies, same for Lessonae and ridibundus
* life : Number of turns left before dying
* reprod_rate : Reproduction rate when meeting a mate, similar for both species
* genet : Lessona are LL, Ridibundus RR, hybrid are LR, LLR or RRL
* sex : Agent sex as a boolean, mother are 1
* mt : Mitochondrias origin, can be L or R depending on the agent's mother
* logger : Agent logger

## Methods

* Move : A frog can one one step in any direction, randomly
* Reproduces : This is obviously the crux of the problem. To reproduce a frog needs to :
    * Meet another frog
    * Of the opposite sex
    * Try to reproduce with success, and have a viable descent. Homotypic crosses are trivial, and LL X RR make hybrids.
    * if "Hybridogenesis" model parameter is set to 1, an LR frog will only make R gameter LR X LL will always make an LR frog, else gamete selection in LR is random
    * if "hybrid_crosses" model parameter is set to 1, hybrid can reproduces with hybrids. This depends on RR frogs origin (see bibliography)
    * if "Preferential mating" is higher than 0, hybrid mate preferentially with RR frogs than LL frogs. This isn't implemented yet
* Dies : After a set number of turns, frogs die and are removed from the model


# Design additional comments

Model based on following article : https://onlinelibrary.wiley.com/doi/full/10.1111/eva.12245#eva12245-bib-0049
With additional ideas from here : https://link.springer.com/article/10.1007/s10530-016-1359-z#Sec20
https://link.springer.com/article/10.1007/s00265-023-03366-y
https://link.springer.com/article/10.1007/s10530-009-9427-2

Design considerations : 

On Hybridogenesis : 

"the majority of diploid hybrid males produced haploid gametes with the P. ridibundus genome after elimination of the P. lessonae genome." -> https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0224759
And
"We noted a male-male competition manifested by the exclusion of young males (before the third hibernation) from the participation in the mating. An analysis of genome transmission to gametes revealed that females P. lessonae transmitted Lx, while P. esculentus transmitted the Rx and/or Lx genomes. Males of P. lessonae transmitted the Lx or Ly genomes, while P. esculentus transmitted the Lx, Ly, and/or Rx genomes. The high proportion of the L genomes transferred to gametes enables both the restoration of the parental species P. lessonae and the regular renewal of a new generation of hybrids." -> https://link.springer.com/article/10.1007/s00265-023-03366-y

On F1 X F1

Crosses between native P. esculentus hybrids are unsuccessful likely due to the expression of recessive deleterious mutations. Introduced individuals coming from Southern Europe, when mating with the native P. lessonae, produce new P. esculentus hybrids that are sterile in all types of crosses (see Fig. 1). In contrast, if the parental P. ridibundus comes from Central Europe, the resulting P. esculentus hybrids are fertile in all types of crosses. -> https://onlinelibrary.wiley.com/doi/full/10.1111/eva.12245#eva12245-bib-0049
