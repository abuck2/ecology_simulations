#using Pkg; Pkg.add("Agents"); Pkg.add("CairoMakie")

using Agents
using Random
using CairoMakie # choosing a plotting backend

#Defines the agents
@agent Frog GridAgent{2} begin
    #Creates a frog that walks on a grid with two dimensions
    genet::String # "LL", "RR" or "LR"
    sex::Bool # The sex of the agent
    mt::Float64 #Rate at which they can reproduce with hybrids
    reprod_rate::Float64 #Rate at which they can reproduce
    life_exp::UInt16 #Life expectancy in turns
    life::UInt16 #Steps left alive
end


#Function to initialize multiples agents in the model
function initialize(; local_agents = 35, invasive_agents = 15, griddims = (20, 20),
        introg_rate = 0.67, reprod_rate_local = 0.8, reprod_rate_invasive = 0.8,
        seed = 125, life_exp=6)
    space = GridSpaceSingle(griddims, periodic = false)
    properties = Dict(:introg_rate => introg_rate) #k:v of properties to pass to the agents
    rng = Random.Xoshiro(seed)

    #Creates the model, with agents, space and scheduler
    model = ABM(
        Frog, space;
        properties, rng, scheduler = Schedulers.Randomly()
    )
    # populate the model with agents, adding equal amount of the two types of agents
    # at random positions in the model
    for n in 1:local_agents
        sex = n % 2 == 1 ? true : false #attempt at ternary evaluation
        agent = Frog(n, (1, 1), 1, sex, 0, reprod_rate_local, life_exp, life_exp)
        add_agent_single!(agent, model)
    end

    for n in 1:invasive_agents
        sex = n % 2 == 1 ? true : false #attempt at ternary evaluation
        agent = Frog(n+local_agents, (1, 1), 0, sex, introg_rate, reprod_rate_invasive, life_exp, life_exp)
        add_agent_single!(agent, model)
    end
    return model
end

function agent_step!(agent, model)

    # Looks for mates
    for neighbor in nearby_agents(agent, model)
        if agent.sex != neighbor.sex
            if agent.locality == 1.0 && neighbor.locality == 1.0
                if rand() < agent.reprod_rate
                    n = nextid(model)+1
                    sex = n % 2 == 1 ? true : false #attempt at ternary evaluation
                    agent = Frog(n, (1, 1), 1, sex, 0, agent.reprod_rate, agent.life_exp, agent.life_exp)
                    add_agent_single!(agent, model)
                end
            elseif agent.locality == 0 && neighbor.locality == 0
                if rand() < agent.reprod_rate
                    n = nextid(model)+1
                    sex = n % 2 == 1 ? true : false #attempt at ternary evaluation
                    agent = Frog(n, (1, 1), 0, sex, agent.introg_rate, agent.reprod_rate, agent.life_exp, agent.life_exp)
                    add_agent_single!(agent, model)
                end
            elseif agent.locality < 1 
                if rand() < agent.introg_rate
                    n = nextid(model)+1
                    sex = n % 2 == 1 ? true : false #attempt at ternary evaluation
                    locality = (agent.locality + neighbor.locality)/2
                    agent = Frog(n, (1, 1), locality, sex, agent.introg_rate, agent.reprod_rate, agent.life_exp, agent.life_exp)
                    add_agent_single!(agent, model)
                end
            end
        end
    end
    
    #Move agent
    randomwalk!(agent, model)

    #Ages
    agent.life = agent.life - 1
    if agent.life < 1 
        remove_agent!(agent, model)
    end
    
    return
end

model = initialize() #initialize the model
step!(model, agent_step!, 3) #advances 3 steps


#Plot test
groupcolor(a) = a.locality == 1 ? :blue : :orange
groupmarker(a) = a.locality == 1 ? :circle : :rect
figure, _ = abmplot(model; ac = groupcolor, am = groupmarker, as = 10)
figure # returning the figure displays it

#Video
model = initialize();
abmvideo(
    "frogs.mp4", model, agent_step!;
    ac = groupcolor, am = groupmarker, as = 10,
    framerate = 4, frames = 120,
    title = "Frogs' introgression model"
)



