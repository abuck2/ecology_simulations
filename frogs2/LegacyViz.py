from FrogsModel import *
from mesa.experimental import JupyterViz
import mesa



def agent_portrayal(agent):
    portrayal = {"Shape": "circle", "Filled": "true", "r": 0.5}
    if agent.genet == "LL":
        portrayal["Color"] = "blue"
        portrayal["Layer"] = 0
    else:
        portrayal["Color"] = "red"
        portrayal["Layer"] = 1
        portrayal["r"] = 0.2
    return portrayal


grid = mesa.visualization.CanvasGrid(agent_portrayal, 20, 20, 500, 500)

chart = mesa.visualization.ChartModule(
    [{"Label": "Ridib", "Color": "Black"}], data_collector_name="datacollector"
)
chartpop = mesa.visualization.ChartModule(
    [{"Label": "Population", "Color": "Black"}], data_collector_name="datacollector"
)

server = mesa.visualization.ModularServer(
    FrogsModel, [grid, chart, chartpop], "Frogs Model", {"n_l": 20, "n_r":5, "n_h":10, "width": 20, "height": 20}
)
server.port = 8521  # The default
server.launch()