from Agent import Frog
from FrogsModel import FrogsModel, compute_population_f, compute_introg
from mesa import batch_run
import matplotlib.pyplot as plt
import seaborn as sns


def run(graphics:bool, steps:int,  n_frogs:int, n_rieuse:int, width:int, height:int, 
            f_rate:float = 0.6, r_rate:float = 0.5, introg:float = 0.6,
            life_exp_f:int = 10, life_exp_r:int = 4):
    model = FrogsModel(n_frogs, n_rieuse, f_rate=f_rate, r_rate=r_rate, introg=introg, life_exp_f=life_exp_f, life_exp_r=life_exp_r,  width=width, height=height)
    for i in range(steps):
        model.step()
    Introg = model.datacollector.get_model_vars_dataframe()
    g = sns.lineplot(data=Introg)
    #plt.show()
    print(Introg)

    plt.savefig("test.jpg")

run(True, steps = 40, n_frogs = 20, n_rieuse = 4, width=10, height=10)