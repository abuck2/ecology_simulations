from mesa import Agent, Model
#Space grid
from mesa.space import MultiGrid
#Scheduler
from mesa.time import RandomActivation
#Data collection
from mesa.datacollection import DataCollector

import matplotlib.pyplot as plt
import numpy as np
import logging
from random import choice



class Frog(Agent):
    
    def __init__(self, unique_id, model, logger, percent_local:float, sex:bool, life_exp:int = 5, reprod_rate:float = 0.05):
        super().__init__(unique_id, model)
        self.unique_id = unique_id
        self.life_exp = life_exp
        self.life = life_exp
        self.reprod_rate = reprod_rate
        self.percent_local = percent_local
        self.sex = sex
        self.logger = logger
        
        
    def step(self):
        #Moves a few random cases
        self.moves()
        
        #Mates
        self.mates()
        
        #Dies
        self.dead()
        
    def moves(self):
        possible_steps = self.model.grid.get_neighborhood(
            tuple(self.pos), moore=True, include_center=False
        )
        new_position = self.random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)
        logging.info('{} moved to {}'.format(self.unique_id, new_position))
        
    def mates(self):
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        cellmates = [obj for obj in cellmates if isinstance(obj, Frog)]
        #self.logger.info("{} friends here".format(len(cellmates)))
        for cellmate in cellmates:
            
            if cellmate.sex != self.sex:
                self.logger.info("Has met an opposite sex mate")
                luck = np.random.uniform(0,1)
                if self.percent_local==1:
                    if cellmate.percent_local==1:
                        self.logger.info("Local")
                        if luck < self.reprod_rate:
                            self.logger.info("And friendly! (rnd : {} < rrate : {})".format(luck, self.reprod_rate))
                            self.make_babies(1)
                        else : 
                            self.logger.info("But not too friendly! (rnd : {} > rrate : {})".format(luck, self.reprod_rate))
                elif self.percent_local==0:
                    if cellmate.percent_local==0:
                        if luck < self.reprod_rate:
                            self.logger.info("And friendly! (rnd : {})".format(luck))
                            self.make_babies(0)
                        else : 
                            self.logger.info("But not too friendly! (rnd : {})".format(luck))
                    else :
                        if luck<self.model.introg:
                            self.logger.info("And friendly! (rnd : {})".format(luck))
                            self.make_babies(cellmate.percent_local)
                        else : 
                            self.logger.info("But not too friendly! (rnd : {})".format(luck))
                        
    def make_babies(self, local_consort):
        sex = choice([True, False])
        new_id = max([agent.unique_id for agent in self.model.schedule.agents])+1
        a = Frog(unique_id = new_id, model = self.model, sex = sex, logger = self.logger, 
                percent_local = (self.percent_local+local_consort)/2, life_exp = self.life_exp, reprod_rate = self.reprod_rate)
        self.model.grid.place_agent(a, self.pos)
        self.model.schedule.add(a)
        self.logger.info("HO WAW!! Frog {} is born and {} local!!".format(a.unique_id, (self.percent_local+local_consort)/2))
        
    def dead(self):
        if self.life == 0:
            self.model.grid.remove_agent(self)
            self.model.schedule.remove(self)
            self.logger.info("Frog {} is dead :'(".format(self.unique_id))

        self.life -= 1
        
class TerrainType(Agent):
    def __init__(self, unique_id, model):
        self.super().__init__(unique_id, model)

        


        
        
