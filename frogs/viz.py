from FrogsModel import *
from mesa.experimental import JupyterViz
import mesa
def agent_portrayal(agent):
    frog = {
        "color": "tab:blue",
        "size": 50,
        "Layer":1,
    }
    if agent.percent_local == 1:
        frog["color"]="tab:blue"
    else :
        frog["color"]="tab:red"

    
    return frog
    
model_params = {
    "n_frogs": {
        "type": "SliderInt",
        "value": 50,
        "label": "Number of local frogs:",
        "min": 10,
        "max": 200,
        "step": 1,
    },
    "n_rieuse": {
        "type": "SliderInt",
        "value": 20,
        "label": "Number of external frogs:",
        "min": 10,
        "max": 200,
        "step": 1,
    },
    "r_rate": {
        "type": "SliderInt",
        "value": 0.5,
        "label": "Reproduction rate of local frogs:",
        "min": 0,
        "max": 1,
        "step": 0.05,
    },
    "f_rate": {
        "type": "SliderInt",
        "value": 0.5,
        "label": "Reproduction rate of alien frogs:",
        "min": 0,
        "max": 1,
        "step": 0.05,
    },
    "introg": {
        "type": "SliderInt",
        "value": 0.1,
        "label": "Introgression rate:",
        "min": 0,
        "max": 1,
        "step": 0.05,
    },
    "life_exp_f": {
        "type": "SliderInt",
        "value": 20,
        "label": "Life expectancy of local frogs:",
        "min": 5,
        "max": 50,
        "step": 1,
    },
    "life_exp_r": {
        "type": "SliderInt",
        "value": 20,
        "label": "Life expectancy of alien frogs:",
        "min": 5,
        "max": 50,
        "step": 1,
    },
    "width": 20,
    "height": 20}


page = JupyterViz(
    FrogsModel,
    model_params,
    measures=["Introg"],
    name="Money Model",
    agent_portrayal=agent_portrayal,
)

