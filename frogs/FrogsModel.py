from mesa import Agent, Model
#Space grid
from mesa.space import MultiGrid
#Scheduler
from mesa.time import RandomActivation
#Data collection
from mesa.datacollection import DataCollector


from numpy import random
import matplotlib.pyplot as plt
import numpy as np
import logging
from scipy.ndimage import gaussian_filter

from Agent import Frog

def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)
    
def compute_population_f(model):
    return len([agent for agent in model.schedule.agents if isinstance(agent, Frog)])

def compute_population_mixed(model):
    frogs = [agent for agent in model.schedule.agents if isinstance(agent, Frog)]
    
    return len([agent for agent in frogs if agent.percent_local<0.9])

def compute_introg(model):
    return np.mean([agent.percent_local for agent in model.schedule.agents if isinstance(agent, Frog)])

class FrogsModel(Model):
    def __init__(self, n_frogs:int, n_rieuse:int, width:int, height:int, 
            f_rate:float = 0.05, r_rate:float = 0.5, introg:float = 0.3,
            life_exp_f:int = 10, life_exp_r:int = 4):
        super().__init__()
        
        
        #Number of humans and vectors
        self.num_frogs = n_frogs
        self.num_rieuse = n_rieuse
        
        self.current_step = 0
        
        #set as model attributes for vizualisation purposes
        self.f_rate = f_rate
        self.r_rate = r_rate
        self.introg = introg
        self.life_exp_f = life_exp_f
        self.life_exp_r = life_exp_r
        
        #Create space
        self.grid = MultiGrid(width, height, True)
        
        #Activation of the agents every step is random
        self.schedule = RandomActivation(self)
        
        #Initialize loggers 
        setup_logger('frogs_logger', "logs/local_frogs.log")
        setup_logger('rieuse_logger', "logs/rieuse.log")
        frogs_logger = logging.getLogger('frogs_logger')
        rieuse_logger = logging.getLogger('rieuse_logger')
        
        self.datacollector = DataCollector(
                model_reporters={
                    "Frogs":compute_population_f, 
                    "Introg":compute_introg,
                    "Mixed":compute_population_mixed
                    },
                 agent_reporters={"PercentLocal": "percent_local"}) #agent-level data
        
        # Create local frogs 
        for i in range(self.num_frogs):
            a = Frog(unique_id = i, model = self, sex = i % 2, logger = frogs_logger,\
                percent_local = 1, life_exp = self.life_exp_f, reprod_rate = self.f_rate)
            self.schedule.add(a)

            #Agent is activated in a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            pos = np.array((x, y))
            self.grid.place_agent(a, pos)
            
        for i in range(self.num_rieuse):
            a = Frog(unique_id = i+self.num_frogs+1, model = self, sex = i % 2, logger = rieuse_logger,\
                percent_local = 0, life_exp = self.life_exp_r, reprod_rate = self.r_rate)
            self.schedule.add(a)

            #Agent is activated in a random space cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            pos = np.array((x, y))
            self.grid.place_agent(a, pos)

    def step(self):
        """Avance the scheduler one step and collect data
        """
        print("Next step")
        #collect data
        self.datacollector.collect(self)
        #advance simulation one step
        self.schedule.step()
        self.current_step += 1
        print(self.current_step)







